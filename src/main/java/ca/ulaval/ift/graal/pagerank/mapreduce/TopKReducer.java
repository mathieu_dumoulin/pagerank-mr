package ca.ulaval.ift.graal.pagerank.mapreduce;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

public class TopKReducer extends Reducer<NullWritable, Text, Text, Text> {
    private static final Logger LOG = LoggerFactory.getLogger(TopKReducer.class);

    private static int DEFAULT_K = 10;
    private int k;
    private final TreeMap<Double, String> topkMap = Maps.newTreeMap();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
	super.setup(context);
	k = context.getConfiguration().getInt("topk", DEFAULT_K);
    }

    @Override
    public void reduce(NullWritable key, Iterable<Text> values, Context context)
	    throws IOException, InterruptedException {
	for (Text value : values) {
	    String[] fields = value.toString().split(";");
	    Double pagerank = Double.parseDouble(fields[0]);
	    String url = fields[1];

	    topkMap.put(pagerank, url);

	    if (topkMap.size() > k) {
		topkMap.remove(topkMap.firstEntry());
	    }
	}

	Map<Double, String> reversed = topkMap.descendingMap();
	Text key2 = new Text();
	Text value = new Text();
	for (Entry<Double, String> entry : reversed.entrySet()) {
	    key2.set(entry.getKey().toString());
	    value.set(entry.getValue());
	    context.write(key2, value);

	    LOG.debug("Key: " + key2 + " value: " + value);
	}
    }
}

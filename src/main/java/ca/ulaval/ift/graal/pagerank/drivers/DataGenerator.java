package ca.ulaval.ift.graal.pagerank.drivers;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobStatus;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class DataGenerator extends Configured implements Tool {
    private static final String URL_PREFIX = "http://www.";

    private static final Logger LOG = LoggerFactory.getLogger(DataGenerator.class);

    private static final String INTPUT_VOCAB_FILENAME = "data/vocab/english_words.txt";
    private static final String OUTPUT_SEQ_FILENAME = "data/pagerank/url_crawl.seq";
    private static final int MAX_URLS = 100;
    private static final int AVERAGE_OUTLINKS = 10;
    private static final int MAX_WORDS_IN_URL = 2;

    private static NormalDistribution norm;
    private static Random random = new Random();

    static final List<String> DOMAINS = Lists.newArrayList(".net", ".com", ".edu");

    public static void main(String[] args) throws Exception {
	int exitCode = ToolRunner.run(new DataGenerator(), args);
	System.exit(exitCode);
    }
    
    /**
     * Idea, seperately generated X% random sites, Y% big portal-like sites 
     * and Y% "content authorities" sites such that X+Y+Z=total generated sites
     */
    @Override
    public int run(String[] arg0) throws IOException {
	List<String> words = Lists.newArrayList();
	List<String> urls = Lists.newArrayList();
	Configuration conf = getConf();

	int averageOutlinksPerUrl = conf.getInt("average.outlinks", AVERAGE_OUTLINKS);
	norm = new NormalDistribution(averageOutlinksPerUrl, averageOutlinksPerUrl / 2);

	int wordCount = 0;
	File vocabFile = new File(conf.get("vocab.path", INTPUT_VOCAB_FILENAME));
	Scanner wordScanner = null;
	try {
	    wordScanner = new Scanner(new FileReader(vocabFile));
	    wordScanner.useDelimiter("\\W");
	    while (wordScanner.hasNext()) {
		String nextWord = wordScanner.next().trim().toLowerCase();
		if (nextWord.length() > 3) {
		    ++wordCount;
		    words.add(nextWord);

		    LOG.debug(wordCount + ": " + nextWord);
		}
	    }
	} finally {
	    if (wordScanner != null)
		wordScanner.close();
	}
	LOG.info("Vocab words read: " + wordCount);

	urls = generateUrls(words, DOMAINS, conf.getInt("url.count", MAX_URLS));

	int urlWrittenCount = 0;
	FileSystem fs = FileSystem.get(conf);
	Path dataPath = new Path(conf.get("url.crawldata.sequence.path", OUTPUT_SEQ_FILENAME));
	LOG.info("Writing output to: " + dataPath.toString());
	
	if (fs.exists(dataPath))
	    fs.delete(dataPath, true);
	
	SequenceFile.Writer writer = null;
	try {
	    writer = SequenceFile.createWriter(fs, conf, dataPath, Text.class, Text.class);
	    urlWrittenCount = writeUrlsToSequenceFile(writer, urls);
	} finally {
	    if (writer != null)
		writer.close();
	}

	LOG.info("Successfully wrote " + urlWrittenCount + " urls");
	return JobStatus.SUCCEEDED;
    }

    private List<String> generateUrls(List<String> words, List<String> domains,
	    int maxUrlsToGenerate) {

	int maxWordsPerUrl = getConf().getInt("max.words_per_url", MAX_WORDS_IN_URL);

	List<String> urls = Lists.newArrayList();
	for (int i = 0; i < maxUrlsToGenerate; i++) {
	    int nWords = random.nextInt(maxWordsPerUrl) + 1;
	    StringBuilder url = new StringBuilder(URL_PREFIX);
	    for (int j = 0; j < nWords; j++) {
		int wordIndex = random.nextInt(words.size());
		url.append(words.get(wordIndex));
	    }
	    url.append(domains.get(random.nextInt(domains.size())));
	    if (urls.contains(url)) {
		--i;
		continue;
	    }
	    urls.add(url.toString());
	}
	return urls;
    }

    private int writeUrlsToSequenceFile(SequenceFile.Writer writer, List<String> urls)
	    throws IOException {
	int urlWrittenCount = 0;

	Text key = new Text();
	Text value = new Text();
	for (String url : urls) {
	    String links = generateListOfLinks(urls, url);

	    key.set(url);
	    value.set(links);
	    writer.append(key, value);

	    urlWrittenCount++;
	    LOG.debug("key: " + key.toString() + " value: " + value.toString());
	}

	return urlWrittenCount;
    }

    private String generateListOfLinks(List<String> urls, String url) {
	int nOutLinks = (int) norm.sample();

	List<String> outlinks = Lists.newArrayList();
	for (int i = 0; i < nOutLinks; i++) {
	    int outLinkIndex = random.nextInt(urls.size());
	    String outlink = urls.get(outLinkIndex);
	    while (outlink.equals(url))
		outlink = urls.get(random.nextInt(urls.size()));
	    if (!outlinks.contains(outlink)) {
		outlinks.add(outlink);
	    }
	}
	StringBuilder sb = new StringBuilder();
	for (String outlink : outlinks) {
	    sb.append(outlink + ";");
	}
	if (sb.length() > 0) {
	    sb.deleteCharAt(sb.length() - 1);
	}
	return sb.toString();
    }
}
